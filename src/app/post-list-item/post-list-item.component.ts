import {Component, Input, OnInit} from '@angular/core';
import {PostsService} from '../services/posts.service';
import {Post} from '../models/post.model';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {
  @Input() post: Post;

  constructor(private postsService: PostsService) {
  }

  ngOnInit() {
  }

  onLoveIt() {
    this.post.loveIts += 1;
    this.postsService.savePosts();
    this.postsService.emitPosts();
  }

  onDontLoveIt() {
    this.post.loveIts -= 1;
    this.postsService.savePosts();
    this.postsService.emitPosts();
  }

  onDelete() {
    this.postsService.removePost(this.post);
  }
}
