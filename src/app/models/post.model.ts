export class Post {
  loveIts: number;
  created_at: Date;
  created_at_JSON: string;
  constructor(public title: string, public content: string) {
    this.loveIts = 0;
    this.created_at = new Date();
    this.created_at_JSON = this.created_at.toJSON();
  }
}
