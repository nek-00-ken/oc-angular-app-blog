import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostListItemComponent } from './post-list-item/post-list-item.component';
import { HeaderComponent } from './header/header.component';
import {RouterModule, Routes} from '@angular/router';
import { PostFormComponent } from './post-form/post-form.component';
import { SinglePostComponent } from './single-post/single-post.component';
import {PostsService} from './services/posts.service';
import {ReactiveFormsModule} from '@angular/forms';

const appRoutes: Routes = [
  { path: 'posts',  component: PostListComponent },
  { path: 'posts/new', component: PostFormComponent },
  { path: 'posts/view/:id', component: SinglePostComponent },
  { path: '', redirectTo: 'posts', pathMatch: 'full' },
  { path: '**', redirectTo: 'posts' }
];


@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    PostListItemComponent,
    HeaderComponent,
    PostFormComponent,
    SinglePostComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
